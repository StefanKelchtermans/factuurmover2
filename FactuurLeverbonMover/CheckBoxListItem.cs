﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace FactuurLeverbonMover
{
    public class CheckBoxListItem
    {
        public string Path { get; set; }
        public string DisplayName { get; set; }
        public string NewName { get; set; }
        public bool IsChecked { get; set; }
        public bool ToXML { get; set; }
        public bool IsFactuur { get; set; }
        public Thickness CheckboxMargin { get; set; }
        public Visibility IsVisible
        {
            get
            {
                if (IsFactuur)
                    return Visibility.Visible;
                else
                    return Visibility.Collapsed;
            }
        }

        public ObservableCollection<CheckBoxListItem> Children { get; set; }

        public CheckBoxListItem()
        {
            this.Children = new ObservableCollection<CheckBoxListItem>();
        }

        public void RemoveChild(int index)
        {
            this.Children.RemoveAt(index);
        }

        internal bool HasBon(List<CheckBoxListItem> list)
        {
            bool result = false;
            foreach (CheckBoxListItem item in Children)
            {
                bool gevonden = false;
                foreach (CheckBoxListItem listitem in list)
                {
                    if (listitem.DisplayName == item.DisplayName)
                    {
                        gevonden = true;
                        break;
                    }
                }
                if (!gevonden)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }
    }
}
