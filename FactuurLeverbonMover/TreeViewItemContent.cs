﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FactuurLeverbonMover
{
    public class TreeViewItemContent
    {
        public string File { get; set; }
        public string Path { get; set; }
        public DateTime Date { get; set; }
        public bool IsChecked { get; set; }
        public bool ToXML { get; set; }
        public bool IsFactuur { get; set; }
        public Visibility IsVisible
        {
            get
            {
                if (IsFactuur)
                    return Visibility.Visible;
                else
                    return Visibility.Collapsed;
            }
        }
    }
}
