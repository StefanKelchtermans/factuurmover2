﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FactuurLeverbonMover
{
    /// <summary>
    /// Interaction logic for WijzigNaamWindow.xaml
    /// </summary>
    public partial class WijzigNaamWindow : Window
    {
        public string NewName { get; set; }
        public WijzigNaamWindow()
        {
            InitializeComponent();
            NewNameTextBox.Focus();
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(NewNameTextBox.Text))
            {
                NewName = NewNameTextBox.Text + ".pdf";
            }
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void NewNameTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (!string.IsNullOrEmpty(NewNameTextBox.Text))
                {
                    NewName = NewNameTextBox.Text + ".pdf";
                }
                DialogResult = true;
            }
            else if (e.Key == Key.Escape)
                DialogResult = false;
        }
    }
}
