﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Collections.ObjectModel;
using System.Threading;
using System.Windows.Threading;
using System.Windows.Media;
using System.Windows.Input;
using System.Globalization;
using System.Timers;
using System.Threading.Tasks;

namespace FactuurLeverbonMover
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string folder { get; set; }
        public string DestinationPath { get; set; }
        public ObservableCollection<CheckBoxListItem> Facturen { get; set; }
        public ObservableCollection<CheckBoxListItem> Leveringsbonnen { get; set; }
        public int maxindex { get; private set; }

        private string PDFType = "";
        private string LoadedFilePath = "";
        private System.Timers.Timer deleteTimer = new System.Timers.Timer(300);
        private string StandaardView = "";
        private bool AppClosing = false;

        public MainWindow()
        {
            Facturen = new ObservableCollection<CheckBoxListItem>();
            Leveringsbonnen = new ObservableCollection<CheckBoxListItem>();
            InitializeComponent();
            deleteTimer.Elapsed += deleteTimer_Elapsed;
            this.DestinationPath = @"C:\Users\stefa\Documents\temp\Testfolder2\";
            StandaardView = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FileMover/StandaardView.htm").Replace("\\", "/");
            SetBasicPage();
        }

        private void SetBasicPage()
        {
            
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { PDFBrowser.Source = null; }, null);
            Uri standaardUri = new Uri(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FileMover/StandaardView.htm"));
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { PDFBrowser.Navigate(standaardUri); }, null);
        }

        private void OpenFolderButton_Click(object sender, RoutedEventArgs e)
        {
            LoadFiles();
        }

        private void TimestampButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string map = StartfoldertextBox.Text;
                if (!string.IsNullOrEmpty(map))
                {
                    DirectoryInfo d = new DirectoryInfo(map);
                    FileInfo[] Files = d.GetFiles("*.pdf");

                    Files = (from f in Files orderby f.CreationTime.Ticks select f).ToArray();
                    string times = "";
                    foreach (FileInfo file in Files)
                    {
                        var time = file.CreationTime.Ticks;
                        times += file.Name + ": " + DateTimeOffset.FromFileTime(time).ToString("dd-MM-yy HH:mm:ss.fff", CultureInfo.InvariantCulture) + "\n";
                    }
                    System.Windows.MessageBox.Show(times);
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Fout bij het tonen van de timestamps!\n" + ex.Message);
            }
        }

        private int GetSortname(FileInfo file)
        {
            if (!file.Name.Contains("_Copy"))
            {
                string result = file.Name.Substring(file.Name.Length - 7, 3);
                int huidigeindex = int.Parse(result);
                if (maxindex - huidigeindex > 500)
                {
                    return 1000 + huidigeindex;
                }
                else
                    return huidigeindex;

            }
            else
                return 20000;
        }

        private  void LoadFiles()
        {
            try
            {
                SetBasicPage();
                DeleteCopyFiles(StartfoldertextBox.Text);
                PDFType = "Facturen";
                Facturen.Clear();
                Leveringsbonnen.Clear();
                string startname = StartNametextBox.Text;
                string notename = BonNameTextBox.Text;
                if (!string.IsNullOrEmpty(StartfoldertextBox.Text))
                    folder = StartfoldertextBox.Text;
                else
                    folder = GetFolder();
                if (!string.IsNullOrEmpty(folder))
                {
                    DirectoryInfo d = new DirectoryInfo(folder);
                    FileInfo[] Files = d.GetFiles("*.pdf");
                    if (Files.Count() > 0)
                    {
                        maxindex = (from f in Files select GetSortname(f)).Max();
                        Files = (from f in Files orderby GetSortname(f) select f).ToArray();
                        CheckBoxListItem item = null;
                        bool flag = false;
                        foreach (FileInfo file in Files)
                        {
                            if (file.Name.Contains("_Copy"))
                            {
                                try
                                {
                                    File.Delete(file.FullName);
                                }
                                catch (Exception)
                                {
                                    continue;
                                }
                            }
                            if (!file.Name.ToLower().Contains(startname) && !file.Name.ToLower().Contains(notename))
                            {
                                flag = false;
                                continue;
                            }
                            if (file.Name.ToLower().Contains(startname))
                            {
                                flag = true;
                                if (item != null)
                                {
                                    Facturen.Add(item);
                                }
                                item = new CheckBoxListItem();
                                item.DisplayName = file.Name;
                                item.Path = file.FullName;
                                item.IsFactuur = true;
                            }
                            else if (file.Name.ToLower().Contains(notename) && item != null)
                            {
                                if (flag)
                                {
                                    CheckBoxListItem child = new CheckBoxListItem();
                                    child.DisplayName = file.Name;
                                    child.Path = file.FullName;
                                    child.IsChecked = false;
                                    child.CheckboxMargin = new Thickness(14, 0,0,0);
                                    item.Children.Add(child);
                                }
                            }
                            else if (file.Name.ToLower().Contains(notename) && item == null)
                            {
                                flag = false;
                                item = new CheckBoxListItem();
                                item.DisplayName = file.Name;
                                item.Path = file.FullName;
                                Facturen.Add(item);
                                item = null;
                            }
                        }
                        if (item != null)
                        {
                            if (!Facturen.Contains(item))
                                Facturen.Add(item);
                        }
                        FacturenTreeView.ItemsSource = Facturen;
                    }
                }
            }
            catch (IOException ioex)
            {
                System.Windows.MessageBox.Show("Fout: " + ioex.Message);
            }
            catch (UnauthorizedAccessException uex)
            {
                System.Windows.MessageBox.Show("Fout: " + uex.Message);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Fout: " + ex.Message);
            }
        }

        private void DeleteCopyFiles(string uri)
        {
            try
            {
                if (!string.IsNullOrEmpty(uri))
                    folder = uri;
                else
                    folder = GetFolder();
                if (!string.IsNullOrEmpty(folder))
                {
                    if (Directory.Exists(folder + "\\temp"))
                    {
                        DirectoryInfo d = new DirectoryInfo(folder + "\\temp");
                        FileInfo[] Files = d.GetFiles("*_Copy.pdf");
                        if (Files.Length > 0)
                        {
                            foreach (FileInfo file in Files)
                            {
                                if (file.Name.Contains("_Copy"))
                                {
                                    try
                                    {
                                        File.Delete(file.FullName);
                                    }
                                    catch (Exception)
                                    {
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async void OpenAndereFolderButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                await Task.Run(() => { SetBasicPage(); });
                PDFType = "Andere";
                Facturen.Clear();
                Leveringsbonnen.Clear();
                folder = GetFolder();
                if (!string.IsNullOrEmpty(folder))
                {
                    DirectoryInfo d = new DirectoryInfo(folder);
                    FileInfo[] Files = d.GetFiles("*.pdf");
                    Files = (from f in Files orderby f.CreationTime.Ticks select f).ToArray();
                    CheckBoxListItem item = null;
                    foreach (FileInfo file in Files)
                    {
                        item = new CheckBoxListItem();
                        item.DisplayName = file.Name;
                        item.Path = file.FullName;
                        Facturen.Add(item);
                    }
                    FacturenTreeView.ItemsSource = Facturen;
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Fout: " + ex.Message);
            }
        }

        private void Reload()
        {
            try
            {
                if (PDFType == "Facturen")
                {
                    Facturen.Clear();
                    DirectoryInfo d = new DirectoryInfo(folder);
                    FileInfo[] Files = d.GetFiles("*.pdf");

                    Files = (from f in Files orderby GetSortname(f) select f).ToArray();
                    CheckBoxListItem item = null;
                    foreach (FileInfo file in Files)
                    {

                        if (file.Name.ToLower().Contains("factuur"))
                        {
                            if (item != null)
                            {
                                Facturen.Add(item);
                            }
                            item = new CheckBoxListItem();
                            item.DisplayName = file.Name;
                            item.Path = file.FullName;
                        }
                        else if (file.Name.ToLower().Contains("ll") && item != null)
                        {
                            CheckBoxListItem child = new CheckBoxListItem();
                            child.DisplayName = file.Name;
                            child.Path = file.FullName;
                            child.IsChecked = false;
                            item.Children.Add(child);
                        }
                        else if (file.Name.ToLower().Contains("ll") && item == null)
                        {
                            item = new CheckBoxListItem();
                            item.DisplayName = file.Name;
                            item.Path = file.FullName;
                        }
                    }
                    if (item != null)
                    {
                        Facturen.Add(item);
                    }
                    FacturenTreeView.ItemsSource = Facturen;
                }
                else if (PDFType == "Andere")
                {
                    PDFType = "Andere";
                    Facturen.Clear();
                    Leveringsbonnen.Clear();
                    DirectoryInfo d = new DirectoryInfo(folder);
                    FileInfo[] Files = d.GetFiles("*.pdf");
                    Files = (from f in Files orderby f.CreationTime select f).ToArray();
                    CheckBoxListItem item = null;
                    foreach (FileInfo file in Files)
                    {
                        item = new CheckBoxListItem();
                        item.DisplayName = file.Name;
                        item.Path = file.FullName;
                        Facturen.Add(item);
                    }
                    FacturenTreeView.ItemsSource = Facturen;
                }

            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Fout: " + ex.Message);
            }
        }

        //private void LoadPDF(CheckBoxListItem item, RoutedEventArgs e)
        //{
        //    SetBasicPage();
        //    DeleteCopyFiles();
        //    string file = item.Path;
        //    string copydfile = file.Substring(0, file.Length - 4) + "_Copy.pdf";
        //    if (!File.Exists(copydfile))
        //        File.Copy(file, copydfile, true);
        //    PDFBrowser.Navigate(new Uri(copydfile));
        //}

        private async void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            await Task.Run(() => { SetBasicPage(); });
            List<string> tenegerenfacturen = GetTeNegerenFacturen();
            if (tenegerenfacturen.Count > 0)
                FacturenToXML(tenegerenfacturen, TargetXMLfoldertextBox.Text);
            if (PDFType == "Facturen")
            {
                
                List<CheckBoxListItem> selected = (from f in Facturen where f.IsChecked == true select f).ToList();
                List<CheckBoxListItem> selectedBonnen = new List<CheckBoxListItem>();
                foreach (CheckBoxListItem factuur in Facturen)
                {
                    foreach (CheckBoxListItem bon in factuur.Children)
                    {
                        if (bon.IsChecked == true)
                        {
                            selectedBonnen.Add(bon);
                        }
                    }
                }
                if (selected.Count > 1)
                    System.Windows.Forms.MessageBox.Show("Gelieve de facturen één voor één te verplaatsen met hun leveringsbon(nen)!");
                else
                {
                    string destination = TargetfoldertextBox.Text;
                    string invoicename = StartNametextBox.Text;
                    string notename = BonNameTextBox.Text;
                    CopyFilesToAK(selected, selectedBonnen, TargetAFfoldertextBox.Text, invoicename, notename);
                    new Thread(delegate () { MoveFiles(selected, selectedBonnen, destination, invoicename, notename); }).Start();
                }
            }
            else if (PDFType == "Andere")
            {
                List<CheckBoxListItem> selected = (from f in Facturen where f.IsChecked == true select f).ToList();
                string targetpath = TargetAnderefoldertextBox.Text;
                string targetrootfolder = GetFolder(targetpath);
                foreach (CheckBoxListItem pdf in selected)
                {
                    string filename = pdf.DisplayName.Substring(0, pdf.DisplayName.Length - 4);
                    string targetfolder = targetrootfolder + "\\" + filename + DateTime.Now.ToBinary().ToString() + ".pdf";
                    string source = pdf.Path;
                    try
                    {
                        if (!File.Exists(targetfolder))
                            File.Move(source, targetfolder);
                    }
                    catch (Exception)
                    {
                        File.Copy(source, targetfolder, true);
                    }
                }
                LoadFiles();
            }
        }

        private List<string> GetTeNegerenFacturen()
        {
            List<string> lijst = new List<string>();
            foreach (CheckBoxListItem item in FacturenTreeView.Items)
            {
                if (item.ToXML)
                    lijst.Add(item.DisplayName);
            }
            return lijst;
        }

        private string GetNewFileName(string file)
        {
            string newname = file.Substring(0, file.Length - 8) + ".pdf";
            return newname;
        }

        private void MoveFiles(List<CheckBoxListItem> facturen, List<CheckBoxListItem> bonnen, string destination, string invoicename, string notename)
        {
            try
            {
                foreach (CheckBoxListItem item in facturen)
                {
                    string factuurpath = item.Path;
                    string factuurfile = item.DisplayName;
                    if (factuurfile.ToLower().Contains(notename))
                    {

                        string target = "";
                        string folder = "";
                        string subfolder = "";
                        string bonfolder = "";
                        // Create the new Folder Name
                        folder = factuurfile.Substring(0, 2);
                        subfolder = factuurfile.Substring(2, 5);
                        bonfolder = factuurfile.Substring(7, 3);
                        string foldername = folder + "\\" + subfolder + "\\" + bonfolder;
                        // Get the main destination directory
                        DestinationPath = destination;
                        if (DestinationPath != "")
                        {
                            // Create the target folder
                            string fileDir = DestinationPath + "\\" + foldername;
                            Directory.CreateDirectory(fileDir);
                            //Move the deliverance file
                            target = fileDir + "\\" + GetNewFileName(factuurfile);
                            if (!File.Exists(target))
                            {
                                try
                                {
                                    File.Move(factuurpath, target);
                                }
                                catch (Exception)
                                {
                                    try
                                    {
                                        File.Copy(factuurpath, target, true);
                                    }
                                    catch (Exception)
                                    {
                                        throw new Exception("Kan het bestand " + factuurfile + " niet vinden!");
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (CheckBoxListItem bon in bonnen)
                        {
                            string filePath = bon.Path;
                            string file = bon.DisplayName;
                            string target = "";
                            bool found = false;
                            int index = 0;
                            foreach (CheckBoxListItem child in item.Children)
                            {
                                if (child.DisplayName == file)
                                {
                                    found = true;
                                    break;
                                }
                                index++;
                            }
                            if (found)
                            {
                                App.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    try
                                    {
                                        Facturen[0].Children.RemoveAt(index);
                                    }
                                    catch (Exception)
                                    {

                                    }
                                });

                            }
                            string folder = "";
                            string subfolder = "";
                            string bonfolder = "";
                            // Create the new Folder Name
                            folder = bon.DisplayName.Substring(0, 2);
                            subfolder = bon.DisplayName.Substring(2, 5);
                            bonfolder = bon.DisplayName.Substring(7, 3);
                            string foldername = folder + "\\" + subfolder + "\\" + bonfolder;
                            // Get the main destination directory
                            DestinationPath = destination;
                            if (DestinationPath != "")
                            {
                                // Create the target folder
                                string fileDir = DestinationPath + "\\" + foldername;
                                Directory.CreateDirectory(fileDir);
                                //Move the deliverance file
                                target = fileDir + "\\" + GetNewFileName(file);
                                if (!File.Exists(target))
                                {
                                    try
                                    {
                                        File.Move(filePath, target);
                                    }
                                    catch (Exception)
                                    {
                                        try
                                        {
                                            File.Copy(filePath, target, true);
                                        }
                                        catch (Exception)
                                        {

                                            throw new Exception("Kan het bestand " + file + " niet vinden!");
                                        }
                                    }
                                }
                            }
                            // copy factuur to that folder
                            target = DestinationPath + "\\" + foldername + "\\" + GetNewFileName(factuurfile);
                            if (!File.Exists(target))
                                File.Copy(factuurpath, target);
                        }
                    }

                    //Delete the invoice from the source folder if there are no more deiverance files left.
                    try
                    {
                        if (!item.HasBon(bonnen))
                            File.Delete(factuurpath);
                    }
                    catch (Exception)
                    {
                        throw new Exception("Kan de factuur " + factuurfile + " niet verwijderen van de beginfolder!");
                    }
                }
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { LoadFiles(); }, null);
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { System.Windows.MessageBox.Show("Bestanden verplaatst."); }, null);
            }
            catch (Exception ex)
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { System.Windows.MessageBox.Show(ex.Message); }, null);
            }
        }

        private async void MoveAllButton_Click(object sender, RoutedEventArgs e)
        {
            List<string> tenegerenfacturen = GetTeNegerenFacturen();
            if (tenegerenfacturen.Count > 0)
                FacturenToXML(tenegerenfacturen, TargetXMLfoldertextBox.Text);
            await Task.Run(() => { SetBasicPage(); });
            if (PDFType == "Facturen")
            {

                CopyAllToAK(TargetAFfoldertextBox.Text);
                string destination = TargetfoldertextBox.Text;
                string startname = StartNametextBox.Text;
                string notename = BonNameTextBox.Text;
                new Thread(delegate () { MoveAllFiles(destination, startname, notename); }).Start();
            }
            else if (PDFType == "Andere")
            {
                string targetpath = TargetAnderefoldertextBox.Text;
                string targetrootfolder = GetFolder(targetpath);
                foreach (CheckBoxListItem pdf in Facturen)
                {
                    string targetfolder = targetrootfolder + "\\" + pdf.DisplayName;
                    string source = pdf.Path;
                    try
                    {
                        if (!File.Exists(targetfolder))
                            File.Move(source, targetfolder);
                    }
                    catch (Exception)
                    {
                        File.Copy(source, targetfolder, true);
                    }
                }
                LoadFiles();
            }
        }

        private void CopyFilesToAK(List<CheckBoxListItem> facturen, List<CheckBoxListItem> bonnen, string destination, string invoicename, string notename)
        {
            try
            {
                foreach (CheckBoxListItem item in facturen)
                {
                    string factuurpath = item.Path;
                    string factuurfile = item.DisplayName;
                    if (factuurfile.ToLower().Contains(invoicename))
                    {
                        string target = "";
                        string folder = "";
                        string subfolder = "";
                        string bonfolder = "";
                        string year = "";
                        string yearfolder = "";
                        // Create the new Folder Name
                        folder = factuurfile.Substring(0, 2);
                        year = factuurfile.Substring(2, 4);
                        subfolder = "000" + factuurfile.Substring(6, 2);
                        bonfolder = factuurfile.Substring(8, 3);
                        yearfolder = "FLEV" + year;
                        string foldername = yearfolder + "\\" + folder + "\\" + subfolder + "\\" + bonfolder;
                        // Get the main destination directory
                        DestinationPath = destination;
                        if (DestinationPath != "")
                        {
                            // Create the target folder
                            string fileDir = DestinationPath + "\\" + foldername;
                            Directory.CreateDirectory(fileDir);

                            //Move the deliverance file
                            foreach (CheckBoxListItem bon in bonnen)
                            {
                                string filePath = bon.Path;
                                string file = bon.DisplayName;
                                //bool found = false;
                                int index = 0;
                                string childpath = fileDir + "\\" + GetNewFileName(bon.DisplayName);
                                if (!File.Exists(target))
                                {
                                    try
                                    {
                                        File.Copy(bon.Path, childpath);
                                    }
                                    catch (Exception)
                                    {
                                        try
                                        {
                                            File.Copy(bon.Path, childpath);
                                        }
                                        catch (Exception)
                                        {
                                            throw new Exception("kan het bestand " + factuurfile + " niet vinden!");
                                        }
                                    }
                                    index++;
                                }

                            }
                            //Move invoice
                            target = fileDir + "\\" + GetNewFileName(factuurfile);
                            if (!File.Exists(target))
                            {
                                try
                                {
                                    File.Copy(factuurpath, target);
                                }
                                catch (Exception)
                                {
                                    try
                                    {
                                        File.Copy(factuurpath, target);
                                    }
                                    catch (Exception)
                                    {
                                        throw new Exception("kan het bestand " + factuurfile + " niet vinden!");
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Fout bij het kopieren van de facturen!\n" + ex.Message);
            }
        }

        private void CopyAllToAK(string destination)
        {
            try
            {
                string invoicename = StartNametextBox.Text;
                string notename = BonNameTextBox.Text;
                foreach (CheckBoxListItem item in Facturen)
                {
                    string factuurpath = item.Path;
                    string factuurfile = item.DisplayName;
                    if (factuurfile.ToLower().Contains(invoicename))
                    {
                        string target = "";
                        string folder = "";
                        string subfolder = "";
                        string bonfolder = "";
                        string year = "";
                        string yearfolder = "";
                        // Create the new Folder Name
                        folder = factuurfile.Substring(0, 2);
                        year = factuurfile.Substring(2, 4);
                        subfolder = "000" + factuurfile.Substring(6, 2);
                        bonfolder = factuurfile.Substring(8, 3);
                        yearfolder = "FLEV" + year;
                        string foldername = yearfolder + "\\" + folder + "\\" + subfolder + "\\" + bonfolder;
                        // Get the main destination directory
                        DestinationPath = destination;
                        if (DestinationPath != "")
                        {
                            // Create the target folder
                            string fileDir = DestinationPath + "\\" + foldername;
                            Directory.CreateDirectory(fileDir);

                            //Move the deliverance file
                            foreach (CheckBoxListItem child in item.Children)
                            {
                                string childpath = fileDir + "\\" + GetNewFileName(child.DisplayName);
                                if (!File.Exists(childpath))
                                {
                                    try
                                    {
                                        File.Copy(child.Path, childpath);
                                    }
                                    catch (Exception ex)
                                    {
                                        try
                                        {
                                            File.Copy(child.Path, childpath);
                                        }
                                        catch (Exception nex)
                                        {
                                            throw new Exception("kan het bestand " + factuurfile + " niet vinden!");
                                        }
                                    }
                                }
                            }
                            //Move invoice
                            target = fileDir + "\\" + GetNewFileName(factuurfile);
                            if (!File.Exists(target))
                            {
                                try
                                {
                                    File.Copy(factuurpath, target);
                                }
                                catch (Exception ex)
                                {
                                    try
                                    {
                                        File.Copy(factuurpath, target);
                                    }
                                    catch (Exception nex)
                                    {
                                        throw new Exception("kan het bestand " + factuurfile + " niet vinden!");
                                    }
                                }
                            }
                        }
                    }
                    //else
                    //{
                    //    foreach (CheckBoxListItem child in item.Children)
                    //    {
                    //        string filePath = child.Path;
                    //        string file = child.DisplayName;
                    //        string target = "";
                    //        string folder = "";
                    //        string subfolder = "";
                    //        string bonfolder = "";
                    //        string year = "";
                    //        string yearfolder = "";
                    //        // Create the new Folder Name
                    //        folder = file.Substring(0, 2);
                    //        year = file.Substring(2, 4);
                    //        subfolder = "000" + file.Substring(6, 2);
                    //        bonfolder = file.Substring(8, 3);
                    //        yearfolder = "FLEV" + year;
                    //        string foldername = yearfolder + "\\" + folder + "\\" + subfolder + "\\" + bonfolder;
                    //        // Get the main destination directory
                    //        DestinationPath = destination;
                    //        if (DestinationPath != "")
                    //        {
                    //            // Create the target folder
                    //            string fileDir = DestinationPath + "\\" + foldername;
                    //            Directory.CreateDirectory(fileDir);
                    //            //Move the deliverance file
                    //            target = fileDir + "\\" + file;
                    //            if (!File.Exists(target))
                    //            {
                    //                try
                    //                {
                    //                    File.Copy(filePath, target);
                    //                }
                    //                catch (Exception)
                    //                {
                    //                    try
                    //                    {
                    //                        File.Copy(filePath, target);
                    //                    }
                    //                    catch (Exception)
                    //                    {
                    //                        throw new Exception("kan het bestand " + file + " niet vinden!");
                    //                    }
                    //                }
                    //            }
                    //        }
                    //        // copy factuur to that folder                      
                    //        target = DestinationPath + "\\" + foldername + "\\" + factuurfile;
                    //        try
                    //        {
                    //            File.Copy(factuurpath, target, true);
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            throw new Exception("Kan de factuur " + factuurfile + " niet vinden!");
                    //        }
                    //    }
                    //}
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Fout bij het kopieren van de facturen!\n" + ex.Message);
            }
        }

        private void MoveAllFiles(string destination, string startname, string notename)
        {
            try
            {
                foreach (CheckBoxListItem item in Facturen)
                {
                    string factuurpath = item.Path;
                    string factuurfile = item.DisplayName;
                    if (factuurfile.ToLower().Contains(notename))
                    {
                        string target = "";
                        string folder = "";
                        string subfolder = "";
                        string bonfolder = "";
                        // Create the new Folder Name
                        folder = factuurfile.Substring(0, 2);
                        subfolder = factuurfile.Substring(2, 5);
                        bonfolder = factuurfile.Substring(7, 3);
                        string foldername = folder + "\\" + subfolder + "\\" + bonfolder;
                        // Get the main destination directory
                        DestinationPath = destination;
                        if (DestinationPath != "")
                        {
                            // Create the target folder
                            string fileDir = DestinationPath + "\\" + foldername;
                            Directory.CreateDirectory(fileDir);
                            //Move the deliverance file
                            target = fileDir + "\\" + GetNewFileName(factuurfile);
                            if (!File.Exists(target))
                            {
                                try
                                {
                                    File.Move(factuurpath, target);
                                }
                                catch (Exception mex)
                                {
                                    try
                                    {
                                        File.Copy(factuurpath, target);
                                    }
                                    catch (Exception cex)
                                    {
                                        throw new Exception("kan het bestand " + factuurfile + " niet vinden!");
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (CheckBoxListItem child in item.Children)
                        {
                            string filePath = child.Path;
                            string file = child.DisplayName;
                            string target = "";
                            string folder = "";
                            string subfolder = "";
                            string bonfolder = "";
                            // Create the new Folder Name
                            folder = child.DisplayName.Substring(0, 2);
                            subfolder = child.DisplayName.Substring(2, 5);
                            bonfolder = child.DisplayName.Substring(7, 3);
                            string foldername = folder + "\\" + subfolder + "\\" + bonfolder;
                            // Get the main destination directory
                            DestinationPath = destination;
                            if (DestinationPath != "")
                            {
                                // Create the target folder
                                string fileDir = DestinationPath + "\\" + foldername;
                                Directory.CreateDirectory(fileDir);
                                //Move the deliverance file
                                target = fileDir + "\\" + GetNewFileName(file);
                                if (!File.Exists(target))
                                {
                                    try
                                    {
                                        File.Move(filePath, target);
                                    }
                                    catch (Exception mex)
                                    {
                                        try
                                        {
                                            File.Copy(filePath, target);
                                        }
                                        catch (Exception cex)
                                        {
                                            throw new Exception("kan het bestand " + file + " niet vinden!");
                                        }
                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        File.Delete(filePath);
                                    }
                                    catch (Exception)
                                    {
                                        throw new Exception("kan het bestand " + file + " niet verwijderen!");
                                    }
                                }
                            }
                            // copy factuur to that folder                      
                            target = DestinationPath + "\\" + foldername + "\\" + GetNewFileName(factuurfile);
                            try
                            {
                                File.Copy(factuurpath, target, true);
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Kan de factuur " + factuurfile + " niet vinden!");
                            }
                        }

                    }
                    //Delete the invoice from the source folder
                    try
                    {
                        File.Delete(factuurpath);
                    }
                    catch (Exception)
                    {
                        throw new Exception("Kan de factuur " + factuurfile + " niet verwijderen van de beginfolder!");
                    }
                }
                // Reload the view
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { Facturen.Clear(); }, null);
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { LoadFiles(); }, null);
                //Leveringsbonnen.Clear();
                //DirectoryInfo d = new DirectoryInfo(folder);
                //FileInfo[] Files = d.GetFiles("*.pdf");
                //Files = (from f in Files orderby GetSortname(f.Name) select f).ToArray();
                //CheckBoxListItem myItem = null;
                //ObservableCollection<CheckBoxListItem> loadedfacturen = new ObservableCollection<CheckBoxListItem>();
                //foreach (FileInfo file in Files)
                //{

                //    if (file.Name.ToLower().Contains(startname))
                //    {
                //        if (myItem != null)
                //        {
                //            loadedfacturen.Add(myItem);
                //        }
                //        myItem = new CheckBoxListItem();
                //        myItem.DisplayName = file.Name;
                //        myItem.Path = file.FullName;
                //    }
                //    else if (file.Name.ToLower().Contains(notename) && myItem != null)
                //    {
                //        CheckBoxListItem child = new CheckBoxListItem();
                //        child.DisplayName = file.Name;
                //        child.Path = file.FullName;
                //        child.IsChecked = false;
                //        myItem.Children.Add(child);
                //    }
                //    else if (file.Name.ToLower().Contains(notename) && myItem == null)
                //    {
                //        myItem = new CheckBoxListItem();
                //        myItem.DisplayName = file.Name;
                //        myItem.Path = file.FullName;
                //    }
                //}
                //if (myItem != null)
                //{
                //    loadedfacturen.Add(myItem);
                //}
                //Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { Facturen = loadedfacturen; }, null);
                //Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { FacturenTreeView.ItemsSource = loadedfacturen; }, null);
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { System.Windows.MessageBox.Show("Bestanden verplaatst."); }, null);
            }
            catch (Exception ex)
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { System.Windows.MessageBox.Show(ex.Message); }, null);
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            try
            {
                SetBasicPage();
                DeleteCopyFiles(StartfoldertextBox.Text);
                System.Windows.Controls.Button button = (System.Windows.Controls.Button)sender;
                string rootpath = StartfoldertextBox.Text;
                string file = button.Tag.ToString();
                string[] fileparts = file.Split('\\');
                string filename = fileparts[(fileparts.Length - 1)];
                string copydfile = rootpath + "\\temp\\" + filename.Substring(0, filename.Length - 4) + "_Copy.pdf";
                if (!Directory.Exists(rootpath + "\\temp"))
                    Directory.CreateDirectory(rootpath + "\\temp");
                if (!File.Exists(copydfile))
                    File.Copy(file, copydfile);
                SetFilePage(new Uri(copydfile));
                //PDFBrowser.Navigate(new Uri(copydfile));
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Fout: " + ex.Message);
            }
        }

        private void SetFilePage(Uri uri)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate
            {
                PDFBrowser.Navigate(uri);
            }, null);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SettingsPanel.Width = 0;
            PDFBrowser.Visibility = Visibility.Visible;
        }

        private void SettingsPanelButton_Click(object sender, RoutedEventArgs e)
        {
            SettingsPanel.Width = 500;
            PDFBrowser.Visibility = Visibility.Hidden;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //AppClosing = true;
            //SetBasicPage();
            //deleteTimer.Start();
            DeleteCopyFiles(StartfoldertextBox.Text);
            this.Close();
        }

        private async void CloseImage_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            AppClosing = true;
            await Task.Run(() => { SetBasicPage(); });
        }

        private void MaximizeButton_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == System.Windows.WindowState.Maximized)
            {
                WindowState = System.Windows.WindowState.Normal;
                //maximizeImage.Source = new BitmapImage(new Uri("pack://application:,,,/PSMWpf;component/Images/maximizeIcon.png"));
            }
            else
            {
                WindowState = System.Windows.WindowState.Maximized;
                //maximizeImage.Source = new BitmapImage(new Uri("pack://application:,,,/PSMWpf;component/Images/normalIcon.png"));
            }
        }

        private void MaximizeImage_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (WindowState == System.Windows.WindowState.Maximized)
            {
                WindowState = System.Windows.WindowState.Normal;
                //maximizeImage.Source = new BitmapImage(new Uri("pack://application:,,,/PSMWpf;component/Images/maximizeIcon.png"));
            }
            else
            {
                WindowState = System.Windows.WindowState.Maximized;
                //maximizeImage.Source = new BitmapImage(new Uri("pack://application:,,,/PSMWpf;component/Images/normalIcon.png"));
            }
        }

        private void MinimizeButton_Click(object sender, RoutedEventArgs e)
        {
            WindowState = System.Windows.WindowState.Minimized;
        }

        private void MinimizeImage_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            WindowState = System.Windows.WindowState.Minimized;
        }

        private void BrowseStartFolder_Click(object sender, RoutedEventArgs e)
        {
            StartfoldertextBox.Text = GetFolder(StartfoldertextBox.Text);
            SaveSettingsButton.IsEnabled = true;
        }

        private string GetFolder(string path)
        {
            try
            {
                FolderBrowserDialog dlg = new FolderBrowserDialog();
                dlg.SelectedPath = path;
                dlg.ShowDialog();
                return dlg.SelectedPath;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GetFolder()
        {
            try
            {
                FolderBrowserDialog dlg = new FolderBrowserDialog();
                dlg.ShowDialog();
                return dlg.SelectedPath;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BrowseTargetFolder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TargetfoldertextBox.Text = GetFolder(TargetfoldertextBox.Text);
                SaveSettingsButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Fout: " + ex.Message);
            }
        }

        private void SaveSettingsButton_Click(object sender, RoutedEventArgs e)
        {
            string startname = StartNametextBox.Text;
            string notename = BonNameTextBox.Text;
            string startfolder = StartfoldertextBox.Text;
            string targetfolder = TargetfoldertextBox.Text;
            string targetOtherfolder = TargetAnderefoldertextBox.Text;
            string targetAFfolder = TargetAFfoldertextBox.Text;
            string targetXMLfolder = TargetXMLfoldertextBox.Text;
            try
            {
                //Uri initPath = new Uri(basepath + "XML/instellingen.xml"); 
                Uri initPath = new Uri(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FileMover/instellingen.xml"));

                XmlDocument instellingen = new XmlDocument();
                instellingen.Load(initPath.LocalPath);
                XmlNode Startname = instellingen.SelectSingleNode("//StartName");
                Startname.InnerXml = startname;

                XmlNode Notename = instellingen.SelectSingleNode("//NoteName");
                Notename.InnerXml = notename;

                XmlNode StartFolder = instellingen.SelectSingleNode("//StartFolder");
                StartFolder.InnerXml = startfolder;

                XmlNode TargetFolder = instellingen.SelectSingleNode("//TargetFolder");
                TargetFolder.InnerXml = targetfolder;

                XmlNode TargetOtherFolder = instellingen.SelectSingleNode("//TargetOtherFolder");
                TargetOtherFolder.InnerXml = targetOtherfolder;

                XmlNode TargetAFFolder = instellingen.SelectSingleNode("//TargetAFFolder");
                TargetAFFolder.InnerXml = targetAFfolder;

                XmlNode XMLName = instellingen.SelectSingleNode("//XMLName");
                XMLName.InnerXml = XMLNameTextBox.Text;

                XmlNode TargetXMLFolder = instellingen.SelectSingleNode("//TargetXMLFolder");
                TargetXMLFolder.InnerXml = targetXMLfolder;

                instellingen.Save(initPath.LocalPath);

                SaveSettingsButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Probleem bij het opslaan van de instellingen!\n" + ex.Message);
            }
        }

        private void FoldertextBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            SaveSettingsButton.IsEnabled = true;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SetSettings();
            DeleteCopyFiles(StartfoldertextBox.Text);
        }

        private void SetSettings()
        {
            try
            {
                Uri initPath = new Uri(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FileMover/instellingen.xml"));

                XmlDocument instellingen = new XmlDocument();
                instellingen.Load(initPath.LocalPath);
                XmlNode Startname = instellingen.SelectSingleNode("//StartName");
                StartNametextBox.Text = Startname.InnerXml;

                XmlNode Notename = instellingen.SelectSingleNode("//NoteName");
                BonNameTextBox.Text = Notename.InnerXml;

                XmlNode StartFolder = instellingen.SelectSingleNode("//StartFolder");
                StartfoldertextBox.Text = StartFolder.InnerXml;

                XmlNode TargetFolder = instellingen.SelectSingleNode("//TargetFolder");
                TargetfoldertextBox.Text = TargetFolder.InnerXml;

                XmlNode TargetOtherFolder = instellingen.SelectSingleNode("//TargetOtherFolder");
                TargetAnderefoldertextBox.Text = TargetOtherFolder.InnerXml;

                XmlNode TargetAFFolder = instellingen.SelectSingleNode("//TargetAFFolder");
                TargetAFfoldertextBox.Text = TargetAFFolder.InnerXml;

                XmlNode XMLName = instellingen.SelectSingleNode("//XMLName");
                XMLNameTextBox.Text = XMLName.InnerXml;

                XmlNode TargetXMLFolder = instellingen.SelectSingleNode("//TargetXMLFolder");
                TargetXMLfoldertextBox.Text = TargetXMLFolder.InnerXml;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Fout: " + ex.Message);
            }
        }

        private void Button_MouseRightButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            WijzigNaamWindow WNW = new WijzigNaamWindow();
            var point = Mouse.GetPosition(System.Windows.Application.Current.MainWindow);
            WNW.Top = point.Y + 140;
            WNW.Left = point.X + 330;
            if (WNW.ShowDialog() == true)
            {
                System.Windows.Controls.Button button = (System.Windows.Controls.Button)sender;

                foreach (CheckBoxListItem item in Facturen)
                {
                    if (item.DisplayName == button.Content.ToString())
                    {
                        try
                        {
                            string oldfilename = button.Tag.ToString();
                            string newFilename = oldfilename.Substring(0, oldfilename.Length - button.Content.ToString().Length) + WNW.NewName;
                            File.Move(oldfilename, newFilename);
                            item.DisplayName = WNW.NewName;
                            button.Content = WNW.NewName;
                            LoadFiles();
                        }
                        catch (Exception)
                        {
                            System.Windows.MessageBox.Show("Kan het bestand niet hernoemen.");
                        }
                        break;
                    }
                }
            }
        }

        private void BrowseTargetAndereFolder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TargetAnderefoldertextBox.Text = GetFolder(TargetAnderefoldertextBox.Text);
                SaveSettingsButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Fout: " + ex.Message);
            }
        }

        private void PDFBrowser_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            //deleteTimer.Start();
        }

        private void deleteTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            deleteTimer.Stop();
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (SendOrPostCallback)delegate { DeletePreviousCopy(); }, null);
        }

        private async void DeletePreviousCopy()
        {
            if (!string.IsNullOrEmpty(LoadedFilePath))
            {
                if (File.Exists(LoadedFilePath))
                {
                    try
                    {
                        if (LoadedFilePath != StandaardView)
                        {
                            File.Delete(LoadedFilePath);
                            if (PDFBrowser.Source != null)
                                LoadedFilePath = PDFBrowser.Source.AbsolutePath;
                            else
                                LoadedFilePath = "";
                        }
                        else
                            LoadedFilePath = "";
                    }
                    catch (Exception)
                    {
                        await Task.Run(() => { SetBasicPage(); });
                    }
                }
            }
            else
            {
                if (PDFBrowser.Source != null && !StandaardView.Equals(PDFBrowser.Source.AbsolutePath))
                    LoadedFilePath = PDFBrowser.Source.AbsolutePath;
                else
                    LoadedFilePath = "";
            }
            if (AppClosing)
                this.Close();
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (WindowState == System.Windows.WindowState.Maximized)
            {
                WindowState = System.Windows.WindowState.Normal;
                //maximizeImage.Source = new BitmapImage(new Uri("pack://application:,,,/PSMWpf;component/Images/maximizeIcon.png"));
            }
            DragMove();
        }

        private void BrowseTargetAFFolder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TargetAFfoldertextBox.Text = GetFolder(TargetAFfoldertextBox.Text);
                SaveSettingsButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Fout: " + ex.Message);
            }
        }

        private void BrowseTargetXMLFolder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TargetXMLfoldertextBox.Text = GetFolder(TargetXMLfoldertextBox.Text);
                SaveSettingsButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Fout: " + ex.Message);
            }
        }


        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            SetSize();
        }

        private void SetSize()
        {
            try
            {
                if (this.WindowState == WindowState.Maximized)
                    this.Height = this.ActualHeight - 70;
            }
            catch (Exception)
            {
            }
        }

        private void FacturenToXML(List<string> facturen, string savepath)
        {
            try
            {
                XmlDocument xml = new XmlDocument();
                XmlDeclaration xmlDeclaration = xml.CreateXmlDeclaration("1.0", "UTF-8", null);
                xml.LoadXml("<Facturen></Facturen>");
                XmlElement root = xml.DocumentElement;
                foreach (string factuur in facturen)
                {
                    string factuurnaam = factuur.Split('-')[0];
                    if (factuurnaam.Contains(".pdf"))
                        factuurnaam = factuurnaam.Substring(0, factuurnaam.IndexOf('.'));
                    XmlNode factuurnode = xml.CreateNode(XmlNodeType.Element, "Factuur", "");
                    factuurnode.InnerText = factuurnaam;
                    root.AppendChild(factuurnode);
                }
                DateTime now = DateTime.Now;
                string filename = "\\" + now.Year.ToString() 
                    + (now.Month < 10 ? "0" + now.Month : now.Month.ToString()) 
                    + (now.Day < 10 ? "0" + now.Day : now.Day.ToString()) 
                    + (now.Hour < 10 ? "0" + now.Hour : now.Hour.ToString())
                    + (now.Minute < 10 ? "0" + now.Minute : now.Minute.ToString())
                    + (now.Second < 10 ? "0" + now.Second : now.Second.ToString())
                    + XMLNameTextBox.Text.Trim() +  ".xml";
                xml.Save(savepath + filename);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Fout bij het creëren van de XML!!\n" + ex.Message);
            }
        }
    }
}
